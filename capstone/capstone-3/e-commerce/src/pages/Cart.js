import React, { useState, useEffect } from "react";
import CartView from "../components/CartView";
import Swal from "sweetalert2";
// import { Container, Card, Button } from "react-bootstrap";

export default function Cart() {
  const [cartItems, setCartItems] = useState([]);

  const fetchData = () => {
    fetch("http://localhost:4000/carts/viewCart", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setCartItems(data.items);
        } else {
          setCartItems(data.items);
        }
      })
      .catch((error) => {
        // Handle any network or other errors here
        console.error("Error :", error);
      });
  };
  useEffect(() => {
    // Fetch the user's cart items from the server and update the state
    fetchData();
  }, []); // Empty dependency array to run the effect once when the component mounts

  const removeFromCart = (productName) => {
    const token = localStorage.getItem("token");

    if (!token) {
      console.log("user is not authenticated");
      return;
    }

    fetch("http://localhost:4000/carts/deleteItem", {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        productName,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          console.log("Product Removed from cart");

          Swal.fire({
            title: "Item removed from cart!",
            icon: "success",
            text: "You have successfully removed this product from your cart",
          });
          fetchData();
        } else {
          Swal.fire({
            title: "Something went wrong!",
            icon: "error",
            text: "There was a problem while removing the product from your cart",
          });
        }
      })
      .catch((error) => {
        console.error(error, "Error Deleting the cart");
      });
  };

  // Update Quantity

  const updateQuantity = (productName, newQuantity) => {
    const token = localStorage.getItem("token");

    if (!token) {
      // Handle the case where the user is not authenticated
      console.log("User is not authenticated.");
      return;
    }

    const requestBody = {
      productName: productName,
      productQuantity: newQuantity,
    };

    fetch(`https://capstone2-triumfante.onrender.com/carts/updateCart`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(requestBody),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          // Handle the case where the server returned an error
          console.log("Failed to update the cart.");
          return null;
        }
      })
      .then((data) => {
        if (data) {
          // If the update is successful, you can handle the updated cart data as needed
          console.log("Cart updated successfully:", data);
          // Perform any client-side updates here
          fetchData();
        }
      })
      .catch((error) => {
        // Handle any network or other errors here
        console.error("Error updating the cart:", error);
      });
  };

  //

  const checkOut = function (productName, productQuantity) {
    const token = localStorage.getItem("token");

    if (!token) {
      console.log("User is not authenticated");
      return;
    }

    const productsToCheckout = [
      {
        productName: productName,
        productQuantity: productQuantity,
      },
    ];

    fetch("https://capstone2-triumfante.onrender.com/carts/checkOutCart", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({ productsToCheckout }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: "Checked Out!",
            icon: "success",
            text: "You Have Succesfully checked out this Product!",
          });
          fetchData();
        } else {
          Swal.fire({
            title: "Please try again!",
            icon: "error",
            text: "There was an error",
          });
        }
      });
  };

  return (
    <>
      <CartView
        // to CartView
        removeFromCart={removeFromCart}
        // to cartview to QuantityUpdater
        updateQuantity={updateQuantity}
        // to CartView
        cartItems={cartItems}
        //  to Cart View
        checkOut={checkOut}
      />
    </>
  );
}
