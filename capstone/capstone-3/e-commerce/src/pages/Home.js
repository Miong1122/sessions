import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
//import CourseCard from '../components/CourseCard';

export default function Home() {
  const data = {
    /* textual contents*/
    title: "Pasok mga Suki",
    content: "Presyong Divisoria",
    /* buttons */
    destination: "/courses",
    label: "Buy now!",
  };

  return (
    <>
      <Banner data={data} />
      <Highlights />
    </>
  );
}
