import { Container, Card, Button } from "react-bootstrap";
import QuantityUpdater from "./QuantityUpdater";

export default function CartView({
  cartItems,
  removeFromCart,
  updateQuantity,
  checkOut,
}) {
  return (
    <Container className="mt-5">
      <h2>Your Cart</h2>
      {cartItems && cartItems.length > 0 ? (
        cartItems.map((item, index) => (
          <Card key={index} className="mb-3">
            <Card.Body>
              <Card.Title>{item.productName}</Card.Title>
              <Card.Subtitle>Quantity:</Card.Subtitle>
              <Card.Text>{item.productQuantity}</Card.Text>
              {/* QuantityUpdater Component */}
              <QuantityUpdater
                productName={item.productName}
                initialQuantity={item.productQuantity}
                onUpdate={updateQuantity}
              />
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {item.price}</Card.Text>
              <Card.Subtitle>Total Price:</Card.Subtitle>
              <Card.Text>PhP {item.totalPrice}</Card.Text>
              <Button
                variant="danger"
                onClick={() => removeFromCart(item.productName)}
              >
                Remove from Cart
              </Button>
              <Button
                variant="primary"
                onClick={() => checkOut(item.productName, item.productQuantity)}
              >
                Check Out
              </Button>
            </Card.Body>
          </Card>
        ))
      ) : (
        <p>Your cart is empty.</p>
      )}
    </Container>
  );
}
