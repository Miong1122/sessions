import { Card, Button } from "react-bootstrap";
import coursesData from "../data/coursesData";
import { useState } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

export default function CourseCard({ courseProp }) {
  // Checks to see if the data was successfully passed
  //console.log(props);
  // Every component recieves information in a form of an object
  //console.log(typeof props);

  const { _id, productName, productDescription, productQuantity, price } =
    courseProp;

  return (
    <Card>
      <Card.Body>
        <Card.Title>{productName}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{productDescription}</Card.Text>
        <Card.Subtitle>Quantity:</Card.Subtitle>
        <Card.Text>{productQuantity}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PhP {price}</Card.Text>
        <Link className="btn btn-primary" to={`/courses/${_id}`}>
          Details
        </Link>
      </Card.Body>
    </Card>
  );
}

// Check if the CourseCard component is getting the correct prop types
CourseCard.propTypes = {
  course: PropTypes.shape({
    productName: PropTypes.string.isRequired,
    productDescription: PropTypes.string.isRequired,
    productQuantity: PropTypes.number.isRequired,
    price: PropTypes.number.isRequired,
  }),
};
