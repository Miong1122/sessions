const User = require(`../models/User`);
const Product = require(`../models/Product`);
const Cart = require(`../models/Cart`);
const Order = require(`../models/Order`);

module.exports.getUserOrders = async (req, res) => {
  try {
    const allOrders = await Order.find({});

    if (!allOrders) {
      return res.send(`There are No Orders Data`);
    } else {
      return res.send(allOrders);
    }
  } catch (error) {
    console.log(error);
  }
  res.send(`There was an error while fetching the Data`);
};
