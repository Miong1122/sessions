const User = require("../models/User");

const Product = require("../models/Product");

const Cart = require("../models/Cart");

const Order = require("../models/Order");

//ADD TO CART

module.exports.addCart = async (req, res) => {
  try {
    if (req.user.isAdmin) {
      return res.send(false);
    }

    // Get user information
    const user = await User.findById(req.user.id);

    if (!user) {
      console.log("Please login");
      return res.send(false);
    }

    let cart = await Cart.findOne({
      userId: req.user.id,
    });

    if (!cart) {
      cart = new Cart({
        userId: req.user.id,
        items: [],
      });
    }

    let productNameToAdd = req.body.productName;
    // Convert user's input to a Number
    let newQuantity = Number(req.body.productQuantity);

    // Retrieve the product from your database with a case-insensitive search
    const productNamePattern = new RegExp(productNameToAdd, "i");
    const product = await Product.findOne({ productName: productNamePattern });

    if (!product) {
      // If the product doesn't exist in the database
      return res.send(`Cannot find this product: ${productNameToAdd}`);
    }

    // Calculate the total price for the new item
    const newPrice = product.price * newQuantity;

    // Find the index of the product in the cart's items array based on the product ID
    const existingProductIndex = cart.items.findIndex(
      (item) => item.productId.toString() === product._id.toString()
    );

    if (existingProductIndex >= 0) {
      // Adding quantity to an existing product in the cart
      cart.items[existingProductIndex].productQuantity += newQuantity;
      // cart.items[existingProductIndex].price += newPrice;
      cart.items[existingProductIndex].totalPrice += newPrice;
    } else {
      if (newQuantity > product.productQuantity) {
        console.log("Product Quantity Exceeds available stocks");
        return res.send("Product Quantity Exceeds available stocks");
      }

      let addToCart = {
        productId: product._id,
        productName: product.productName,
        productQuantity: newQuantity, // Use the newQuantity here
        price: product.price, // Set the calculated price
        totalPrice: newPrice,
      };
      cart.items.push(addToCart);
    }

    cart.totalPrice = cart.items.reduce(
      (total, item) => total + item.totalPrice,
      0
    );

    await cart.save();
    console.log("Product Added to Cart");
    return res.send(true);
  } catch (error) {
    console.log(error);

    return res.status(500).json({ message: "Internal Server Error!" });
  }
};

// View Cart

module.exports.viewCart = async (req, res) => {
  try {
    const cart = await Cart.findOne({ userId: req.user.id });

    if (!cart) {
      console.log("Cart not found");
      return res.send(false);
    }

    // Retrieve the user's name based on the userId
    const user = await User.findById(req.user.id);

    if (!user) {
      console.log("User not found");
      return res.send(false);
    }

    // Update the cart object to include the userName
    const cartWithUserName = {
      ...cart.toObject(), // Convert the cart to a plain JavaScript object
      userName: user.fullName, // Replace 'fullName' with the actual field storing the user's name
    };

    return res.send(cartWithUserName);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

// module.exports.viewCart = async (req, res) => {
//   const cartItems = await Cart.find();

//   if (cartItems.length === 0) {
//     res
//       .status(500)
//       .json({ success: false, message: "No Products available on the cart" });
//   }
//   res.send(cartItems);
// };

//Update Cart
module.exports.updateCart = async (req, res) => {
  try {
    const userId = req.user.id;

    const newQuantity = Number(req.body.productQuantity);
    const productNameToUpdate = req.body.productName;

    const cart = await Cart.findOne({ userId });
    if (!cart) {
      console.log("Cart not found");
      return res.status(404).send(false);
    }

    const productIndex = cart.items.findIndex((item) =>
      new RegExp(`^${productNameToUpdate}$`, "i").test(item.productName)
    );

    console.log(productIndex);
    if (productIndex === -1) {
      console.log("Product Not found in the cart");
      console.log(productIndex);
      return res.send(false);
    }
    cart.items[productIndex].productQuantity = newQuantity;
    await cart.save();
    console.log("Cart Successfully Updated");
    res.send(true);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

//Delete item in the Cart
module.exports.deleteItem = async (req, res) => {
  try {
    // Get the user's cart
    const cart = await Cart.findOne({ userId: req.user.id });

    if (!cart) {
      console.log(`Cart not found`);
      return res.send(null);
    }

    const productNameToDelete = req.body.productName;

    // Find the index of the product in the cart's items array based on the product name (case-insensitive)
    const productIndexToDelete = cart.items.findIndex((item) =>
      new RegExp(`^${productNameToDelete}$`, "i").test(item.productName)
    );

    if (productIndexToDelete === -1) {
      console.log(`Product not found in the cart`);
      return res.send(false);
    }

    // Remove the entire product from the cart
    cart.items.splice(productIndexToDelete, 1);

    // Save the updated cart
    await cart.save();

    console.log(`Product removed from the cart`);
    return res.send(true);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: `Internal Server Error` });
  }
};

//Delete all Cart

module.exports.deleteCart = async (req, res) => {
  try {
    const userId = req.user.id;

    const deleteCart = await Cart.findOneAndDelete({ userId });
    if (!deleteCart) {
      res.send("No Item Found");
    }
    return res.send("DELETED");
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

// CHECK OUT

// Check out cart and push it to the Orders
module.exports.checkOut = async (req, res) => {
  try {
    // Get the user's cart
    const cart = await Cart.findOne({ userId: req.user.id });

    if (!cart) {
      console.log(`Cart is Empty`);
      return res.send(null);
    }

    // Calculate the total cart amount and totalCartPrice
    let totalCartAmount = 0; // Initialize totalCartAmount to zero
    let totalCartPrice = 0; // Initialize totalCartPrice to zero

    // Update product stocks and create an array to track updated products
    const myProducts = [];

    for (const requestedItem of req.body.productsToCheckout) {
      // Case-insensitive search for the product within the cart's items
      const item = cart.items.find((cartItem) =>
        new RegExp(`^${requestedItem.productName}$`, "i").test(
          cartItem.productName
        )
      );

      if (!item) {
        console.log(`product not found in cart`);
        return res.send(
          `Product not found in cart: ${requestedItem.productName}`
        );
      }

      // Find the product from the database using a case-insensitive query
      const product = await Product.findOne({
        productName: new RegExp(`^${requestedItem.productName}$`, "i"),
      });

      // if (!product) {
      //   return res.send(`Product not found: ${requestedItem.productName}`);
      // }

      if (requestedItem.productQuantity > item.productQuantity) {
        return res.send(
          `Requested quantity exceeds cart quantity for ${requestedItem.productName}`
        );
      }

      if (requestedItem.productQuantity > product.productQuantity) {
        return res.send(`Insufficient stock for ${requestedItem.productName}`);
      }

      // Update product stock
      product.productQuantity -= requestedItem.productQuantity;
      await product.save();

      // Deduct the quantity from the cart
      item.productQuantity -= requestedItem.productQuantity;

      // Calculate the total price for the current item
      const itemPrice = product.price * requestedItem.productQuantity;

      // Add the updated product to the array with quantity
      myProducts.push({
        productId: product._id,
        productName: product.productName,
        updatedQuantity: requestedItem.productQuantity,
        price: itemPrice, // Add the price to the myProducts array
        productQuantity: requestedItem.productQuantity, // Add the quantity to the myProducts array
      });

      // Update the total cart amount and totalCartPrice
      totalCartAmount += itemPrice;
      totalCartPrice += itemPrice;
    }

    // Save the billing address to the user's details
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.send(`User not found`);
    }

    user.billingAddress = req.body.billingAddress; // Update the billingAddress field

    await user.save();

    // Clear the user's cart of items with quantity <= 0
    cart.items = cart.items.filter((item) => item.productQuantity > 0);

    // Check if the cart is empty and delete it if it is
    if (cart.items.length === 0) {
      await Cart.findOneAndDelete({ userId: req.user.id });
    } else {
      // Save the updated cart
      await cart.save();
    }

    // Create an order object for the current checkout
    const order = {
      userId: req.user.id,
      items: myProducts, // The array of updated products from the cart
      purchasedOn: new Date(),
      totalCartPrice: totalCartPrice, // Add totalCartPrice to the order
    };
    await Order.findByIdAndUpdate(
      req.user.id, // Adjust this query to match your use case
      { $push: { activeOrders: order } },
      { new: true, upsert: true }
    );

    // Return the user's name, updated products, and total cart amount
    return res.json({
      myProducts,
      totalCartAmount,
      message: `Checkout successful`,
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: `Internal Server Error` });
  }
};
