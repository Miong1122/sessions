// [ SECTION ] Dependecies and Modules
const User = require("../models/User");

const bcrypt = require("bcrypt");

const auth = require("../auth.js");

//User Registation

// module.exports.regUser = (req, res) => {
//   let newUser = new User({
//     firstName: req.body.firstName,
//     lastName: req.body.lastName,
//     email: req.body.email,
//     password: bcrypt.hashSync(req.body.password, 10),
//   });
//   return newUser
//     .save()
//     .then((user, error) => {
//       if (error) {
//         return res.send(false);
//       } else {
//         return res.send(true);
//       }
//     })
//     .catch((error) => res.send(error));
// };
//register user
module.exports.regUser = async (req, res) => {
  try {
    const newUser = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      mobileNo: req.body.mobileNo,
      password: bcrypt.hashSync(req.body.password, 10),
    });

    const userEmail = await User.findOne({ email: req.body.email });
    if (userEmail) {
      console.log("Email already exist");
      return res.send(false);
    }

    await newUser.save();
    console.log("Account successfully registered");
    return res.send(true);
  } catch (error) {
    console.log(error);
    res.send(false);
  }
};

// GET ALL USER

module.exports.getUser = (req, res) => {
  return User.find({}).then((result) => {
    if (result.length === 0) {
      return res.send("No Registered User");
    } else {
      return res.send(result);
    }
  });
};

//[SECTION] Retrieve user details

module.exports.getProfile = (req, res) => {
  return User.findById(req.user.id)
    .then((result) => {
      result.password = "";
      return res.send(result);
    })
    .catch((err) => res.send(err));
};

// User Login

module.exports.loginUser = (req, res) => {
  return User.findOne({ email: req.body.email }).then((result) => {
    if (result === null) {
      return res.send("User not found!");
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        req.body.password,
        result.password
      );
      if (isPasswordCorrect) {
        return res.send({ access: auth.createAccessToken(result) });
      } else {
        return res.send("Password Incorect");
      }
    }
  });
};

module.exports.updateUser = (req, res) => {
  let activateAdmin = {
    isAdmin: true,
  };
  return User.findByIdAndUpdate(req.params.id, activateAdmin)
    .then((result, error) => {
      if (result) {
        return res.send("Successfully updated");
      } else {
        return res.send("Access denied!");
      }
    })
    .catch((error) => res.send(error));
};

module.exports.changeAdmin = (req, res) => {
  let deActivateAdmin = {
    isAdmin: false,
  };
  return User.findByIdAndUpdate(req.params.id, deActivateAdmin)
    .then((result, error) => {
      if (result) {
        return res.send("Successfully updated");
      } else {
        return res.send("Access denied!");
      }
    })
    .catch((error) => res.send(error));
};

// Update user details

module.exports.updateUserDetails = (req, res) => {
  let updatedDetails = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
  };
  return User.findByIdAndUpdate(req.params.id, updatedDetails)
    .then((result, error) => {
      if (result) {
        return res.send("Successfully updated");
      } else {
        return res.send(error);
      }
    })
    .catch((error) => res.send(error));
};

//Reset Password

module.exports.resetPassword = async (req, res) => {
  try {
    const newPassword = req.body.newPassword;
    const userId = req.user.id;

    if (!newPassword || newPassword.trim() === "") {
      return res.status(400).json({ message: "New password is required" });
    }

    // Hash the new password
    const hashedPassword = await bcrypt.hash(newPassword, 12);

    await User.findByIdAndUpdate(userId, { password: hashedPassword });

    res.status(200).json({ message: "Password Reset successfully" });
  } catch (error) {
    res.status(500).json({ message: `Internal Server Error` });
  }
};

module.exports.updateProfile = async (req, res) => {
  try {
    console.log(req.user);
    console.log(req.body);

    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, mobileNo } = req.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo },
      { new: true }
    );

    res.send(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: "Failed to update profile" });
  }
};
