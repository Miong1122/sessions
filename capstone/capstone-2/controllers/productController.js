// [ SECTION ] Dependecies and Modules
const Product = require("../models/Product");

const bcrypt = require("bcrypt");

const auth = require("../auth.js");

//Add product
module.exports.addProduct = (req, res) => {
  const newProduct = new Product({
    productName: req.body.productName,
    productDescription: req.body.productDescription,
    price: req.body.price,
    productQuantity: req.body.productQuantity,
  });

  return newProduct
    .save()
    .then((product, error) => {
      if (error) {
        res.send(false);
      } else {
        res.send(true);
        console.log(product);
      }
    })
    .catch((error) => error);
};

// GET all product

module.exports.allProduct = (req, res) => {
  return Product.find({})
    .then((allProduct) => {
      if (allProduct.length === 0) {
        console.log("No Product to display");
        return res.send(false);
      } else {
        console.log(allProduct);
        return res.send(allProduct);
      }
    })
    .catch((error) => res.send(error));
};

//[SECTION] Retrieving a specific course
module.exports.getProduct = (req, res) => {
  return Product.findById(req.params.productId)
    .then((result) => {
      return res.send(result);
    })
    .catch((err) => res.send(err));
};

//Update Product

module.exports.updateProduct = (req, res) => {
  let updatedProduct = {
    productName: req.body.productName,
    productDescription: req.body.productDescription,
    price: req.body.price,
    productQuantity: req.body.productQuantity,
  };
  return Product.findByIdAndUpdate(req.params.productId, updatedProduct)
    .then((result, error) => {
      if (result) {
        return res.send(true);
      } else {
        return res.send(false);
      }
    })
    .catch((error) => res.send(error));
};

//Archive Product

module.exports.archiveProduct = (req, res) => {
  let archiveProduct = {
    isActive: false,
  };

  return Product.findByIdAndUpdate(req.params.productId, archiveProduct)
    .then((product, error) => {
      if (error) {
        return res.send(false);
      } else {
        return res.send(true);
      }
    })
    .catch((error) => res.send(error));
};

// Activate Product

module.exports.activateProduct = (req, res) => {
  let activateProduct = {
    isActive: true,
  };

  return Product.findByIdAndUpdate(req.params.productId, activateProduct)
    .then((product, error) => {
      if (error) {
        return res.send(false);
      } else {
        return res.send(true);
      }
    })
    .catch((error) => res.send(error));
};

//GET ALL ACTIVE PRODUCT

module.exports.allActive = (req, res) => {
  return Product.find({ isActive: true })
    .then((allActiveProduct) => {
      if (allActiveProduct.length === 0) {
        return res.send("No Product to display");
      } else {
        return res.send(allActiveProduct);
      }
    })
    .catch((error) => res.send(error));
};

//seacrh product by name

module.exports.searchProductsByName = async (req, res) => {
  try {
    const searchProduct = await Product.find({
      $or: [
        { productName: { $regex: new RegExp(req.body.productName, "i") } },
        { category: { $regex: new RegExp(req.body.productName, "i") } },
      ],
    }).sort({ productName: -1 }); // Sort by productName in descending order

    if (searchProduct.length > 0) {
      console.log(`${searchProduct}`);
      return res.send(searchProduct); // Products exist
    } else {
      console.log("Product doesn't exist");
      return res.send([]); // Product doesn't exist
    }
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({ error: "Internal Server Error" });
  }

  // try {
  //   const { productName } = req.body;

  //   // Use a regular expression to perform a case-insensitive search
  //   const products = await Product.find({
  //     name: { $regex: productName, $options: "i" },
  //   });

  //   res.json(products);
  // } catch (error) {
  //   console.error(error);
  //   res.status(500).json({ error: "Internal Server Error" });
  // }
};
