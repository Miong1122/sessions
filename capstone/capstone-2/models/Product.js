const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  productName: {
    type: String,
    required: [true, "Product name is required"],
  },

  productDescription: {
    type: String,
    required: [true, "Product description is required"],
  },
  price: {
    type: Number,
    required: [true, "Price is required"],
  },
  productQuantity: {
    type: Number,
    required: [true, "Quantity is required"],
  },
  receiveProductOn: {
    type: Date,
    default: new Date(),
  },
  isActive: {
    type: Boolean,
    default: true,
  },
});

module.exports = mongoose.model("Product", productSchema);
