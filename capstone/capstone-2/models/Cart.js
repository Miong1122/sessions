const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, "userId is Required"],
  },
  items: [
    {
      productId: {
        type: String,
        required: [true, "productId is required"],
      },
      productName: {
        type: String,
        required: [true, "productName is rquired"],
      },
      productDescription: {
        type: String,
      },
      productQuantity: {
        type: Number,
        required: [true, "ProductQuantity is required"],
      },
      price: {
        type: Number,
        required: [true, "Price is required"],
      },
      totalPrice: {
        type: Number,
        // required: [true, "Total Price is required"],
      },
    },
  ],
  totalAmount: {
    type: Number,
  },
  purchasedOn: {
    type: Date,
    dafault: new Date(),
  },
});

cartSchema.methods.calculateTotalPrice = function () {
  this.items.forEach((item) => {
    item.totalPrice = item.productQuantity * item.price;
  });
};

cartSchema.pre("save", function (next) {
  this.calculateTotalPrice();
  this.totalAmount = this.items.reduce(
    (total, item) => total + item.totalPrice,
    0
  );
  next();
});

module.exports = mongoose.model("Cart", cartSchema);
