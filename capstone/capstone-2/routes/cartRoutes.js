// [ SECTION ] Dependencies and Modules
const express = require("express");

// [ SECTION ] Routing Component
const router = express.Router();

const cartController = require("../controllers/cartController");

const auth = require("../auth.js");

//Destructive from auth

const { verify, verifyAdmin } = auth;

// Add to Cart

router.post("/addCart", verify, cartController.addCart);

// View all cart

router.get("/viewCart", verify, cartController.viewCart);

//Update Cart

router.put("/updateCart", verify, cartController.updateCart);

//Delete item in the cart

router.delete("/deleteItem", verify, cartController.deleteItem);

// Delete entire cart

router.delete("/deleteCart", verify, cartController.deleteCart);

//Check out cart
router.post("/checkOutCart", verify, cartController.checkOut);

module.exports = router;
