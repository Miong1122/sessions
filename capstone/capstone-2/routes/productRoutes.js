// [ SECTION ] Dependencies and Modules
const express = require("express");

// [ SECTION ] Routing Component
const router = express.Router();

const productController = require("../controllers/productController");

const auth = require("../auth.js");

//Destructive from auth

const { verify, verifyAdmin } = auth;

//Add product
router.post("/addProduct", verify, verifyAdmin, productController.addProduct);

// Retrieve all product

router.get("/allProduct", productController.allProduct);

//[SECTION] Route for retrieving a specific course
router.get("/:productId", productController.getProduct);

// Update Product

router.put(
  "/updateProduct/:productId",
  verify,
  verifyAdmin,
  productController.updateProduct
);

// Archive Product

router.put(
  "/:productId/archiveProduct",
  verify,
  verifyAdmin,
  productController.archiveProduct
);
//Activate Product
router.put(
  "/:productId/activateProduct",
  verify,
  verifyAdmin,
  productController.activateProduct
);

//Get all active Product

router.get("/allActive", verify, productController.allActive);

//[SECTION] Route for Search Course by Name
router.post("/searchByName", productController.searchProductsByName);

// [ SECTION ] Export Route System
module.exports = router;
