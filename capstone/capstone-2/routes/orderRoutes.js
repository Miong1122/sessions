// [ SECTION ] Dependencies and Modules
const express = require("express");

// [ SECTION ] Routing Component
const router = express.Router();

const orderController = require("../controllers/orderController");

const auth = require("../auth.js");

//Destructive from auth

const { verify, verifyAdmin } = auth;

router.get("/userOrder", verify, orderController.getUserOrders);

module.exports = router;
