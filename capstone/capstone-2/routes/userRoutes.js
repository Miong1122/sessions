// [ SECTION ] Dependencies and Modules
const express = require("express");

// [ SECTION ] Routing Component
const router = express.Router();

const userController = require("../controllers/userController");

const auth = require("../auth.js");

//Destructive from auth

const { verify, verifyAdmin } = auth;

// User Registration
router.post("/registration", userController.regUser);

//ghet user profile

router.get("/details", verify, userController.getProfile);

//Get all registered user

router.get("/getAllUser", userController.getUser);

// User Login

router.post("/login", userController.loginUser);

//Update user to admin

router.put("/user-admin/:id", verify, verifyAdmin, userController.updateUser);

// //Update admin to user

router.put("/admin-user/:id", verify, verifyAdmin, userController.changeAdmin);

//Change user details

router.put("/updateDetails/:id", userController.updateUserDetails);

//[SECTION] Reset Password
router.put("/reset-password", verify, userController.resetPassword);

//[SECTION] Update Profile
router.put("/profile", verify, userController.updateProfile);

// [ SECTION ] Export Route System
module.exports = router;
