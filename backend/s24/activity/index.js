// console.log("Hello World");


//Objective 1
//Add code here
//Note: function name is numberLooper


    function numberLooper(numberUser){
        
        for(let x = numberUser; x > 0; x--){
        
            if(x <= 50){
                console.log("The current value is at: " + x + " Terminating the loop.");
                break;
            }
          else if(x % 10 === 0){
                console.log("The number is divisible by 10. Skipping The number");
                continue;
            }
    
           else if(x % 5 === 0){
                console.log(x);
            }
            
    
            
        }
    }
    let numberUser = Number(prompt("Give me a number"));
    console.log("The number you provided is " + numberUser);
    numberLooper(numberUser);



    // let number = Number(prompt("Give me a number"));
    // console.log("The number you provided is " + number);

    // for(let x = number; x > 0; number--){
    
    //   if(x % 10 === 0){
    //         console.log("The number is divisible by 10. Skipping The number");
    //         continue;
    //     }

    //    if(x % 5 === 0){
    //         console.log(x);
    //     }
    //     if(x===50){
    //         console.log("The current value is at 50. Terminating the loop.");
    //         break;
    //     }

        
    // }






//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

for(let x = 0; x < string.length; x++){
    let letter = string[x].toLowerCase();
    if("aeiou".includes(letter)){
        continue;
    }

    filteredString += letter;
}

console.log(filteredString);

//Add code here





//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null,
        numberLooper: typeof numberLooper !== 'undefined' ? numberLooper : null
    }
} catch(err){

}