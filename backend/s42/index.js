const express = require("express");

const mongoose = require("mongoose");

const taskRoute = require ("./routes/taskRoute.js");

// Server set up + midlleware

const app = express();
const port = 4000;

app.use(express.json());

app.use(express.urlencoded({extented: true}));

app.use("/tasks", taskRoute);

//DB Connection

mongoose.connect("mongodb+srv://admin:admin123@cluster0.urg3n0n.mongodb.net/B305-to-do?retryWrites=true&w=majority",
{
    useNewUrlParser : true,
    useUnifiedTopology : true

    //allows us to avoid any current o future errors while connectiong to mongoDB
}
);


let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error!"));

//If connection is successful, output in console

db.once("open", () => console.log("We're connection to the cloud database."));


//Server listening

if(require.main ===module){
    app.listen(port, ()=> console.log(`Server running at port ${port}`));

}