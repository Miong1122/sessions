const express = require("express");
//allows access to HTTP Methods and niddlewares
const router = express.Router();

const taskController = require ("../controllers/taskController.js");

//Get all task
router.get("/", (req,res)=>{
    taskController.getAllTask().then (resultFromController => 
        res.send(resultFromController));
});

//Create Task

router.post("/", (req, res) => {
    taskController.createTask(req.body).then (resultFromController => 
        res.send(resultFromController));
})


//Delete a task using wildcard on params
// ":" -> wildcard

router.delete("/:id", (req, res) => {
    taskController.deleteTask(req.params.id).then (resultFromController => 
        res.send(resultFromController));
})


// Update task

router.put("/:id", (req, res) => {
    taskController.updateTask(req.params.id, req.body).then (resultFromController => 
        res.send(resultFromController));
})


//specific task

router.get("/:id", (req, res) => {
    taskController.getSpecificTask(req.params.id, req.body).then((resultFromController) => res.send(resultFromController));
  });



  // changing status to complete

  router.put("/:id/complete", (req, res) => {
    taskController.taskToComplete(req.params.id).then((resultFromController) => res.send(resultFromController));
  });

module.exports = router;

