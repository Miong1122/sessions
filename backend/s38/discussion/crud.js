const http = require(`http`);
const port = 4000;

// Mock Database
let directory = [
  {
    name: "Brandon",
    email: "brandon@mail.com",
  },
  {
    name: "Brandon",
    email: "brandon@mail.com",
  },
];

http
  .createServer(function (request, response) {
    if (request.url === `/users` && request.method === `GET`) {
      response.writeHead(200, { "Content-Type": "application/json" });
      response.write(JSON.stringify(directory));
      response.end();
    }

    if (request.url === `/users` && request.method === `POST`) {
      let requestBody = ``;
      console.log(requestBody);

      request.on(`data`, function (data) {
        requestBody += data;

        console.log("This is from request.on(data)");
        console.log(requestBody);
      });

      request.on(`end`, function () {
        requestBody = JSON.parse(requestBody);

        let newUser = {
          name: requestBody.name,
          email: requestBody.email,
        };
        directory.push(newUser);

        response.writeHead(200, { "Content-Type": "application/json" });
        response.write(JSON.stringify(newUser));
        response.end();
      });
    }
  })
  .listen(port);

console.log(`Server is running at localhost:${port}!`);
