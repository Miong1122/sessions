//Parameters and Agruments

function printName(name){
    console.log("My name is " + name);
}
// Calling or Invoking our Function "printName"
printName("Juana");
printName("Jayson");

let sampleVariable = "John";

printName(sampleVariable);

function checkDivisibility(num){
let remainder = num % 8;
console.log("The remainder of " + num + " divided by 8 is: " + remainder);
let isDivisibleBy8 = remainder === 0;
console.log("Is " + num + " divisible by 8?");
console.log(isDivisibleBy8);
}

//Invocation
checkDivisibility(64);



//FUnction Agruments

function agrumentFunction(){
    console.log("This function was passed as an agrument")
}

function invokeFunction(agrument){
    agrumentFunction();
}

invokeFunction();

//Multiple Parameters in a Function

function createFullName(FirstName, middleName, LastName){
    console.log(FirstName + " " + middleName + " " + LastName);

}

//Multiple Agruments in an Invocation

createFullName("Juan", "Dela", "Cruz");
createFullName("Juan", "Dela");
createFullName("Juan", "Dela", "Cruz", "hello");

//FUntion with Alert Message
function showSampleAlert(){
    alert("Hellow User!");
}

// showSampleAlert();
console.log("Testing");


// prompt()

let samplePrompt = prompt("Enter Your Name");

console.log("Hello, " + samplePrompt);

//Funtion with Prompts

function printWelcomeMessage(){
    let FirstName = prompt("Enter your First Name");
    let LastName = prompt ("ENter your Last Name");
    console.log("Hello, " + FirstName + " " + LastName + "!");
    console.log("Welcome to my page!");
}

printWelcomeMessage();

function greeting(){
    return "Hello, This is the return statement";
}
let greetingFunction = greeting();
console.log(greetingFunction);
    

