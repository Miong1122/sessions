// [ SECTION ] Dependencies and Modules
const express = require('express');


// [ SECTION ] Routing Component
const router = express.Router();

const userController = require('../controllers/user');

const auth = require("../auth.js");


//Destructive from auth

const {verify, verifyAdmin} = auth;

// check email routes
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});



// User registration routes

router.post("/register", (req, res)=>{
	userController.registerUser(req.body).then (resultFromController =>
		res.send (resultFromController))
});



// user authentication

// router.post("/login", userController.loginUser);
router.post(`/login`, (req, res) => {userController.loginUser(req.body)
	  .then((resultFromController) => res.send(resultFromController));
  });
  

/* router.post(`/findUser`, verify, (req, res) => {
	userController.getOneUser(req.body).then((resultFromController) => res.send(resultFromController));
  }); */


  router.get("/details", verify, userController.getProfile);


  //Enroll user to a course

  router.post("/enroll", verify, userController.enroll);


  //routes
router.get(`/getEnrollments`, verify, userController.getEnrollments);

// Reset Password
router.put("/reset-password", verify, userController.resetPassword);



// [ SECTION ] Export Route System
module.exports = router;