// [ SECTION ] Dependencies and Modules
const express = require('express');

// [ SECTION ] Routing Component
const router = express.Router();

const userController = require('../controllers/user');

const courseController = require('../controllers/course');

const auth = require("../auth.js");


//Destructive from auth

const {verify, verifyAdmin} = auth;


router.post("/", verify, verifyAdmin, courseController.addCourse);

//GET all courses

router.get("/all", courseController.getAllCourses);


// GET all ""ACtive course"

router.get("/", courseController.getAllActive);

//Get specific course using its id

router.get("/:courseId", courseController.getCourse);

//Updating a course (Admin only)

router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

//Routes
// Archiving a Course(Admin Only)

router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse)

//Activating a Course (Admin Only)

router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse)




module.exports = router;