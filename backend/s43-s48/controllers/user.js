// [ SECTION ] Dependecies and Modules
const User = require('../models/User');

const Course = require('../models/Course');

const bcrypt = require("bcrypt");

const auth = require("../auth.js");

// check email exists
/*
	Steps:
		1. Use mongoose 'find' method to find duplicate emails
		2. Use the 'then' method to send a response back to the frontend application based on the result of the "find" method.
*/
module.exports.checkEmailExist = (reqBody) => {
	console.log(reqBody);
	return User.find({ email: reqBody.email }).then(result => {
		console.log(result);
		if(result.length > 0){
			return true
		} else {
			return false
		};
	});
};


//User registration

/*

Steps:
1. Create a new User object using the mongoose
 model and the information from the requst body
*/

module.exports.registerUser = (reqBody) => {
	console.log(reqBody);
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then ((user, error)=>{
		if(error){
			return false
		}else{
			return true
		}
	})
	.catch((error) => error);
};

// user authentication.


module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then((result) => {
	  console.log(result);
	  if (result === null) {
		return false;
	  } else {
		const isPasswordCorrect = bcrypt.compareSync(
		  reqBody.password,
		  result.password
		);
		console.log(isPasswordCorrect);
  
		if (isPasswordCorrect) {
		  return { access: auth.createAccessToken(result) };
		} else {
		  return false;
		}
	  }
	});
  };
  
  

// module.exports.loginUser = (req, res) => {
// 	return User.findOne({ email: req.body.email}).then(result => {
// 		console.log(result);
// 		if (result == null){
// 			return res.send(false) // the email doesnt exist in our DB
// 		}else {
// 			const isPasswordCorrect = bcrypt.compareSync(req.body.password,
// 				result.password);

// 				if (isPasswordCorrect){
// 					return res.send ({ access: auth.createAccessToken(
// 						result)})
					
// 				}else{
// 					return res.send (false); //Passwords do not match
// 				}
// 		}
// 	})
// 	.catch(err => res.send(err))

// }


/* module.exports.getOneUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then((result) => {
	  if (result) {
		result.password = ``;
	  } else {
		return `User does not exist`;
	  }
	  return result;
	});
  }; */

  module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id)
	  .then((result) => {
		result.password = "";
		return res.send(result);
	  })
	  .catch((error) => error);
  };


  // ENROLL

  module.exports.enroll = async (req, res) => {
	console.log(req.user.id);
	console.log(req.body.courseId);

	if(req.user.isAdmin){
		return res.send("You cannot do this action")
	}

	let isUserUpdated = await User.findById(req.user.id)
	.then(user => {
		let newEnrollment = {
			courseId: req.body.courseId,
			courseName: req.body.courseName,
			courseDescription: req.body.courseDescription,
			coursePrice: req.body.coursePrice
		}

		user.enrollments.push(newEnrollment);
		return user.save().then(user => true).catch(error => res.send(error));
	})

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	let isCourseUpdated = await Course.findById(req.body.courseId)
	.then(course => {
		let enrolee = {
			userId: req.user.id,

		}

		course.enrollees.push(enrolee);
		return course.save().then(course => true).catch(error => res.send(error));
	})

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated});
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send("You are now enrolled to a course. Thank you")
	}



  }


  module.exports.getEnrollments = (req, res) => {
	return User.findById(req.user.id).then((result) => {
	  if (result.isAdmin) {
		return res.send(`You cannot do this action`);
	  }
  
	  if (result.enrollments.length < 1) {
		return res.send(`No Courses Enrolled`);
	  } else {
		return res.send(result.enrollments);
	  }
	});
  };


  module.exports.resetPassword = async (req, res) => {
	try{
		const newPassword = req.body.newPassword;
		const userId = req.user.id;

		// Hashing the new password
		const hashedPassword = await bcrypt.hash(newPassword, 10);

		await User.findByIdAndUpdate(userId, {password: hashedPassword});

		res.status(200).json({message: "Password reset successfully!"});
	}catch(error){
		res.status(500).json({message: "Internal Server Error"});
	}
}
