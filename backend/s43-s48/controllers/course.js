// [ SECTION ] Dependecies and Modules
const User = require('../models/User');
const Course = require('../models/Course');


const bcrypt = require("bcrypt");

const auth = require("../auth.js");


module.exports.addCourse = (req, res) => {
	
	const newCourse = new Course ({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
	});

	return newCourse.save().then ((course, error)=>{
		if(error){
			res.send (false);
		}else{
			res.send(true);
		}
	})
	.catch((error) => error);
};


module.exports.getAllCourses = (req, res) => {
    return Course.find({}).then (result => {
        if(result.length == 0){
            return res.send("There is no courses in the DB");
        }else{
            return res.send(result);
        }
    })
}


module.exports.getAllActive = (req, res) => {
    return Course.find({isActive : true}).then (result => {
       if(result.length == 0){
            return res.send("There is currentlyno active Courses");
        }else{
            return res.send(result);
        }
    })
}

module.exports.getCourse = (req, res) => {
    return Course.findById(req.params.courseId).then(result => {
        // if(result === 0){
        //     return res.send("Cannot find course with the procided ID.");
        // }else{
        //     return res.send(result);
        // }

        if (result === 0){
            return res.send("Cannot find course with the procided ID.");
        }else{
            if(result.isActive === false){
                return res.send("The course you are trying to access is not available");
            }else{
                return res.send(result);
            }
        }
    })
    .catch(error => res.send("Please enter a correct course ID"));
}

// TRY - CATCH - FINALLY



module.exports.updateCourse = (req, res) => {
    let updatedCourse = {
        name: req.body.name,
        decription: req.body.description,
        price: req.body.price,
    }
    return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course, error) => {
        if (error){
            return res.send(false);
        }else{
            return res.send(true);
        }
    })
}

module.exports.archiveCourse = (req, res) =>{
    let archiveCourse = {
        isActive : false
    }
    return Course.findByIdAndUpdate(req.params.courseId, archiveCourse).then((course,error) =>{
        if(error){
            return res.send(false);
        }else{
            return res.send(true);
        }
    })
}

module.exports.activateCourse = (req, res) =>{
    let activateCourse = {
        isActive : true
    }
    return Course.findByIdAndUpdate(req.params.courseId, activateCourse).then((course,error) =>{
        if(error){
            return res.send(false);
        }else{
            return res.send(true);
        }
    })
}