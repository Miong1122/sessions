// 

console.log("Hellow World");
// conosle.log("Hellow again");
console.log("Goodbye");

// When certain statements take a lot of time to process, this slows down our code
// An example of this are when loops are used on a large amount of information or when fetching data from databases
// When an action will take some time to process, this results in code  "blocking"


// for (let i = 0; i <= 1500; i++){
//     console.log(i);
// }

console.log("Hello again");


//fetch("URL").then((response) => {})

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

fetch("https://jsonplaceholder.typicode.com/posts")
.then(response=> console.log(response.status));


fetch("https://jsonplaceholder.typicode.com/posts")
.then(response=> response.json())
.then(json => console.log(json));

async function fetchData(){

    let result = await fetch("https://jsonplaceholder.typicode.com/posts");

    console.log(result);
    console.log(typeof result);
    console.log(result.body);

    let json = result.json();
    console.log(json);


}

fetchData();

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(response=> response.json())
.then(json => console.log(json));


// [SECTION] Creating a post

/*
ffetch("URL", options)
.then(response->{})
.then(response->{})
*/

fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    headers: {"Content-Type" : "application/json"},
    body: JSON.stringify({
        title: "New Post",
        body: "Hellow World!",
        userId: 1
    })

})
.then(response => response.json())
.then(json => console.log(json));

// Updating a post


fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PUT",
    headers: {"Content-Type" : "application/json"},
    body: JSON.stringify({
        title: "Updated Post",
        body: "Hellow Again!",
        userId: 1
    })

})
.then(response => response.json())
.then(json => console.log(json));


