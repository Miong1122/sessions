function countLetter(letter, sentence) {
  let result = 0;

  // Check first whether the letter is a single character.
  // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
  // If letter is invalid, return undefined.
  if (letter.length !== 1) {
    return undefined;
  }
  for (let i = 0; i < sentence.length; i++) {
    // if (letter[i] === sentence) {
    //   result++;
    //   console.log(result);
    //   return result;
    if (sentence[i] === letter) {
      result++;
    }
  }
  return result;
}

const test = countLetter("o", "The quick brown fox jumps over the lazy dog");
console.log(test);

function isIsogram(text) {
  // An isogram is a word where there are no repeating letters.
  // The function should disregard text casing before doing anything else.
  // If the function finds a repeating letter, return false. Otherwise, return true.

  text = text.toLowerCase();

  const nonRepeat = [];

  for (let i = 0; i < text.length; i++) {
    const letter = text[i];
    console.log(text[i]);

    if (nonRepeat[letter]) {
      return false;
    }
    nonRepeat[letter] = true;
  }
  return true;
}

const word = "Hello";
console.log(isIsogram(word));

function purchase(age, price) {
  // Return undefined for people aged below 13.
  // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
  // Return the rounded off price for people aged 22 to 64.
  // The returned value should be a string.

  const discountedPrice = (price * 0.8).toFixed(2);

  if (age < 13) {
    return undefined;
  } else if (age >= 13 && age <= 21) {
    return discountedPrice;
  } else if (age >= 65) {
    return discountedPrice;
  } else {
    return price.toFixed(2);
  }
}

console.log(purchase(12, 100));

function findHotCategories(items) {
  // Find categories that has no more stocks.
  // The hot categories must be unique; no repeating categories.
  // The passed items array from the test are the following:
  // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
  // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
  // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
  // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
  // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
  // The expected output after processing the items array is ['toiletries', 'gadgets'].
  // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
  const categoryMap = {};
  const hotCategories = [];

  for (const product of items) {
    const { category, stocks } = product;

    if (!categoryMap[category] && stocks === 0) {
      categoryMap[category] = true;

      if (hotCategories.indexOf(category) === -1) {
        hotCategories.push(category);
      }
    }
  }
  return hotCategories;
}

const product = [
  { id: "tltry001", name: "soap", stocks: 14, category: "toiletries" },
  { id: "tltry002", name: "shampoo", stocks: 8, category: "toiletries" },
  { id: "tltry003", name: "tissues", stocks: 0, category: "toiletries" },
  { id: "gdgt001", name: "phone", stocks: 0, category: "gadgets" },
  { id: "gdgt002", name: "monitor", stocks: 0, category: "gadgets" },
];

const test1 = findHotCategories(product);
console.log(test1);

function findFlyingVoters(candidateA, candidateB) {
  // Find voters who voted for both candidate A and candidate B.
  // The passed values from the test are the following:
  // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
  // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
  // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
  // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
  const setA = new Set(candidateA);
  const setB = new Set(candidateB);

  const commonVoters = [];

  for (const voter of setA) {
    if (setB.has(voter)) {
      commonVoters.push(voter);
    }
  }
  return commonVoters;
}

const candidateA = ["LIWf1l", "V2hjZH", "rDmZns", "PvaRBI", "i7Xw6C", "NPhm2m"];
const candidateB = ["kcUtuu", "LLeUTl", "r04Zsl", "84EqYo", "V2hjZH", "LIWf1l"];

const test3 = findFlyingVoters(candidateA, candidateB);
console.log(test3);

module.exports = {
  countLetter,
  isIsogram,
  purchase,
  findHotCategories,
  findFlyingVoters,
};
